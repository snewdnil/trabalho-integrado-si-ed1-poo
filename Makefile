# Makefile

# Trabalho Integrado: ED1/POO/SI
#
# Integrantes:
# Henrique Teruo Eihara      RA 490016
# Marcello Acar              RA 552550
# Rafael Ferraz Zanetti      RA 552380
# Ângela Rodrigues Ferreira  RA 552070
# Eraldo Gonçalves Junoir    RA 489620

projeto:
	g++ main.cpp vendaClasse.cpp sessaoClasse.cpp ingressoClasse.cpp salaClasse.cpp fileiraClasse.cpp assentoClasse.cpp -o trabalho
rm:
	rm trabalho 
