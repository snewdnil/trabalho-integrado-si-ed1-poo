/*
	Implementação dos métodos da classe Assento.h
*/

#include "assentoClasse.h"



bool Assento::verificaDisponibilidade(){
	if(disponibilidade)
		return true;
	else
		return false;
}

void Assento::setDisponibilidade(int status){
	disponibilidade = status;
}

void Assento::setIdAssento(int id){
	idAssento = id;
}
void Assento::setIdFileira(char id){
	idFileira = id;
}
int Assento::getIdAssento(){
	return idAssento;
}
