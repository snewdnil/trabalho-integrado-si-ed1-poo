/*UNIVERSIDADE FEDERAL DE SÃO CARLOS - CAMPUS SOROCABA
Trabalho Integrado POO/ED1/ISI - 1º/2014

Código referente a classe Assento.
*/
#ifndef ASSENTO_H
#define ASSENTO_H

class Assento{

private:
	int idAssento;
	bool disponibilidade;
	char idFileira;

public:
	Assento(){} // Construtor Padrão
	~Assento(){}
	bool verificaDisponibilidade(); ///verifica se o assento esta disponivel
	void setIdAssento(int id); //Atualiza o id do Assento
	void setIdFileira(char id);	//Atualiza o id Fileira a qual o Assento pertence
	void setDisponibilidade(int status); //Atualiza a disponibilidade do assento.
	int getIdAssento();
};

#endif