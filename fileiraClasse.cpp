/*
	UNIVERSIDADE FEDERAL DE SÃO CARLOS - CAMPUS SOROCABA
	Trabalho Integrado POO/ED1/ISI - 1º/2014

	Implementação dos métodos da classe fileira
*/


#include "fileiraClasse.h"
#include <iostream>
using namespace std;

#define DISPONIVEL 1
#define INDISPONIVEL 0

//Destrutor de Fileira --- desaloca o vetor de assentos
Fileira::~Fileira(){
	delete [] assentos;
}
//Inicializa os atributos dos assentos dessa fileira
void Fileira::criaAssentos(){
	int i;
	try{
		assentos = new Assento[qtdAssentos]; //Aloca o vetor de assentos
		for (i=0; i < qtdAssentos; i++){
			assentos[i].setIdFileira(idFileira);
			assentos[i].setIdAssento(i + 1); //Considerei que o id dos Assentos serao os proprios indices do vetor de assentos
			assentos[i].setDisponibilidade(DISPONIVEL); // Coloca todos os assentos como disponiveis
		}
	}catch(bad_alloc){
		cout<<"Erro! Nao foi possivel alocar os Assentos da fileira"<<endl;
		throw bad_alloc();
	}
}

//Verifica se o assento escolhido da fileira esta livre
bool Fileira::verificaDisponibilidade(int numAssento){ 
	if(assentos[numAssento].verificaDisponibilidade())
		return true;
	else
		return false;
}

void Fileira::setIdFileira(char id){
	idFileira = id;
}
void Fileira::setQtdAssentos(int num){
	qtdAssentos = num;
}

char Fileira::getIdFileira() const{
	return idFileira;
}

Assento * Fileira::getAssentos() const{
	return assentos;
}

int Fileira::getQtdAssentos() const{
	return qtdAssentos;
}
