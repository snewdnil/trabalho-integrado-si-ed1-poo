/*
UNIVERSIDADE FEDERAL DE SÃO CARLOS - CAMPUS SOROCABA
Trabalho Integrado POO/ED1/ISI - 1º/2014

Código referente a classe Fileira
*/
#ifndef FILEIRA_H
#define FILEIRA_H

#include "assentoClasse.h"

class Fileira{

private:
	char idFileira;
	Assento *assentos; //Para fazer a alocacao dinamica do vetor de assentos
	int qtdAssentos;
public:
	Fileira(){}
	~Fileira();
	bool verificaDisponibilidade(int numAssento); //Verifica a disponibilidade do assento
	void criaAssentos(); //Inicializa os atributos do assento
	void setQtdAssentos(int num); // Modifica o numero de assentos da fileira
	void setIdFileira(char id); // Modifica o id da fileira
	int getQtdAssentos() const; //Retorna a qtd de assentos
	Assento* getAssentos() const; //Retorna um ponteiro para o vetor de assentos
	char getIdFileira() const;	//So para testes.
};

#endif
