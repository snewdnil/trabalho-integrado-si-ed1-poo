#include "ingressoClasse.h"

Assento * Ingresso::getAssentoAtual(){
  return assentoIngresso;
}

Sessao * Ingresso::getSessaoAtual(){
  return sessaoIngresso;
}

Ingresso::Ingresso(Assento * assentoParam, Sessao * sessaoParam){
  setDtIngresso();
  setValor();
  assentoIngresso = assentoParam;
  sessaoIngresso = sessaoParam;
}

tm Ingresso::getDtIngresso()
{
	return *date;
}

double Ingresso::getValor()
{
  if (tipo == meia)
    return valor/2;
  else
    return valor;
}

void Ingresso::setDtIngresso()
{
  int resp;

  t = time(0);
  date = localtime(&t);

  date->tm_year+=1900;
  date->tm_mon+=1;

  cout << "Hora: " << date->tm_hour << "Minutos: " << date->tm_min << "Segundos: " << date->tm_sec << endl;
  cout << date->tm_mday << "/" << date->tm_mon << "/" << date->tm_year << endl;
  cout << "Confirma horário e data a ser colocada no ingresso?" << endl;
  cout << "1 - Sim" << endl;
  cout << "2 - Não" << endl;
  cin >> resp;

  if (resp == 2)
  {
    cout << "Hora: " << endl;
    cin >> Ingresso::date->tm_hour;
    cout << "Minutos: " << endl;
    cin >> Ingresso::date->tm_min;
    cout << "Dia: " << endl;
    cin >> Ingresso::date->tm_mday;
    cout << "Mes: " << endl;
    cin >> Ingresso::date->tm_mon;
    cout << "Ano: " << endl;
    cin >> Ingresso::date->tm_year;
  }
}

void Ingresso::setValor()
{
  cout << "Informe o tipo do ingresso\n1- Meia\n0- Inteira\nEntrada: " << endl;
  cin >> tipo;
  cout << "Informe o valor do ingresso a ser vendido: " << endl;
  cin >> valor;
}
