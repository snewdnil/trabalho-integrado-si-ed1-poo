#ifndef INGRESSO_H
#define INGRESSO_H

#include <iostream>
#include <ctime>
#include <string>
#include "sessaoClasse.h"
#include "assentoClasse.h"

using namespace std;

class Ingresso
{
	private:
		enum tipoIngresso{
			meia = 1,
			inteira
		};
      	int tipo;
		double valor;
    	time_t t;
		tm * date;
		Assento * assentoIngresso;
	    Sessao * sessaoIngresso;

  public:
	  Ingresso(Assento * assentoParam, Sessao * sessaoParam);
	  tm getDtIngresso();
	  double getValor();
	  void setDtIngresso();
	  void setValor();
    Assento * getAssentoAtual();
    Sessao * getSessaoAtual();
};

#endif
