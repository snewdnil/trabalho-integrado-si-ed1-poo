/* Trabalho Integrado: ED1/POO/SI
//
// Integrantes:
// Henrique Teruo Eihara      RA 490016
// Marcello Acar              RA 552550
// Rafael Ferraz Zanetti      RA 552380
// Ângela Rodrigues Ferreira  RA 552070
// Eraldo Gonçalves Junoir    RA 489620
//
// make         - para compilar
// make rm      - para remover
// ./trabalho     para executar
*/

#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include "salaClasse.h"
#include "vendaClasse.h"
#include "sessaoClasse.h"
#include "ingressoClasse.h"
#define MAX_SALAS 50 //definindo o número máximo de salas


using namespace std;

enum Situacao{
    disponivel = 0,
		manuEquipamento,
		reforma,
		manuGeral,
		desativada
};

typedef struct noingresso{
  Ingresso * info;
  struct noingresso *prox;
  // construtor da struct
  noingresso(){
    prox = NULL;
    info = NULL;
  }
}NoIngresso;

typedef struct nosessao{
  Sessao * info;
  struct nosessao * prox;
  nosessao(){
    prox = NULL;
    info = NULL;
  }
}NoSessao;

typedef struct novenda{
  Venda * info;
  struct novenda * prox;
  novenda(){
    prox = NULL;
    info = NULL;
  }
}NoVenda;

//int carregarArquivo(NoVenda ** venda, NoSessao ** sessao, Sala ** salas);
void gerarArquivo(NoVenda ** listaVenda);
int VendaControle(NoSessao ** listaSessao,NoVenda ** listaVenda);
int VendaControleIngresso(Venda * venda, Sessao * sessao);
int GerenciarSalas(Sala** salas, int * qtdSala);
Sessao * VendaControleEscolherSessao(NoSessao ** listaSessao);
void GerenciarSessao(NoSessao **listaSessao, Sala **salas, int qtdSala);
void editarSessao(Sessao *sessao, Sala **salas, int qtdSala);
void exibirSessao(Sessao *sessao);
void inicializaSala(Sala ** sala);

int verificaArquivo(char const * c){
  ifstream f(c);
  if(f.good()){
    f.close();
    return 1;
  }else{
    f.close();
    return 0;
  }
}

int main(){
  int input;
  int i;
  NoVenda * venda = NULL;
  NoSessao * sessao = NULL;
  Sala ** salas = NULL;

  //if(carregarArquivo(&venda, &sessao,salas)){
    // caso carregue o arquivo....
  //}else{
    salas = new Sala *[50];
    for(i=0;i<MAX_SALAS;i++){
      salas[i] = new Sala(i,0,4,0);
    }

    inicializaSala(salas);
 // }
  int qtdSala = 0;

  do{
    system("clear");
    cout << "1 - Gerenciar Vendas" << endl;
    cout << "2 - Gerenciar Sessão" << endl;
    cout << "3 - Gerenciar Salas" << endl;
    cout << "Qualquer outra opção - Sair" << endl;
    cout << "Entrada: ";
    cin >> input;

    switch(input){
      case 1:
          VendaControle(&sessao,&venda);
        break;
      case 2:
        GerenciarSessao(&sessao, salas, qtdSala);
        break;
      case 3:
        GerenciarSalas(salas, &qtdSala);
        break;
      default:
        input = 0;
        break;
    }
  }while(input);

  gerarArquivo(&venda);

  for(i=0; i<MAX_SALAS; i++){
    delete salas[i];
  }
  delete salas;

  delete venda;
  delete sessao;

  return 0;
}

int VendaControle(NoSessao ** listaSessao,NoVenda ** listaVenda){
  if(*listaSessao == NULL){
    system("clear");
    cout << "Lista sessão está vazia..." << endl;
    system("sleep 2");
    return 0;
  }

  NoVenda * aux = NULL;
  Sessao * auxiliar = NULL;
  tm dtVenda;

  int input;
  int input2;
  char input3;

  do{
    system("clear");
    cout << "1 - Realizar uma venda" << endl;
    cout << "2 - Editar uma venda" << endl;
    cout << "Qualquer outra opção - Sair" << endl;
    cout << "Entrada: ";

    cin >> input;

    switch(input){
      case 1:
        system("clear");
        auxiliar = VendaControleEscolherSessao(listaSessao);
        if(auxiliar != NULL){
          if((*listaVenda) == NULL){
            try{
              (*listaVenda) = new NoVenda();
              (*listaVenda)->info = new Venda(auxiliar);
              VendaControleIngresso((*listaVenda)->info, auxiliar);
            }catch(bad_alloc){
              cout << "NÃO FOI POSSÍVEL CRIAR UMA NOVA VENDA" << endl ;
            }
          }else{
            aux = (*listaVenda);
            while(aux->prox != NULL){
              aux = aux->prox;
            }
              aux->prox = new NoVenda();
              aux->prox->info = new Venda(auxiliar);
              VendaControleIngresso((*listaVenda)->info, auxiliar);
          }
        }
        break;
      case 2:
        aux = (*listaVenda);
          if(aux != NULL){
            do{
              system("clear");

              dtVenda = aux->info->getDtVenda();
              cout << "VENDA DATA " << dtVenda.tm_mday <<"/"<< dtVenda.tm_mon << "/" << dtVenda.tm_year << endl; 
              cout << "VALOR TOTAL: " << aux->info->getValorTotal() << endl;
              cout << "1: Editar venda" << endl << "2: Próxima venda" << endl << "Qualquer outra tecla: Sair" << endl << "Entrada: ";
              cin >> input2;

              if(input2 == 1){
                VendaControleIngresso(aux->info,aux->info->getSessao());
                input2 = 0;
              }else{
                if(input2 == 2){
                  if(aux->prox == NULL){
                    system("clear");
                    cout << "NÃO HÁ MAIS VENDA DISPONÍVEL..." << endl;
                    system("sleep 2");
                    break;
                    input2 = 0;
                  }else{
                    aux = aux->prox;
                  }
                }else{
                  input2 = 0;
                }
              }
            }while(input2);
          }else{
            cout << "LISTA DE VENDA VAZIA!!" << endl;
          }
        break;
      default:
        input = 0;
        break;
    }
  }while(input);
}

Assento * VendaControleEscolherAssento(Sessao * sessao){
  Sala * aux = sessao->getSalaAtual();
  Fileira * auxFileira = aux[0].getFileiras();
  int fileiras = aux->getQtdFileiras();
  int qtdassentos = auxFileira[0].getQtdAssentos();
  int i, i2;
  int flag = 1;

  do{
      system("clear");
      cout << "SALA - " << aux->getNumSala() << endl;
      char c = 65;

      cout << endl;
      for(i=0;i<fileiras;i++){
        for(i2=0;i2<qtdassentos;i2++){
        if(auxFileira[i].verificaDisponibilidade(i2) == true){
            cout << " " << i<<"-" << i2 << "o"  <<  " " ;
          }else{
            cout << " " << i << "-" << i2 << "x"  <<  " " ;
          }
        }
        cout << endl;
      }
      cout << endl;
      cout << "Legenda: " <<endl<< "Primeiro Numero - Fileira " <<endl<< "Segundo Numero - Assento " <<endl<< "Letra - o Disponivel x Indisponivel" << endl;
      cout << "---------------------------------------------" << endl;
      int a,b;
      cout << "Digite a fileira em que pertence seu assento: ";
      cin >> a;
      cout << "Digite a coluna em que pertence seu assento: ";
      cin >> b;

      if(aux->getFileiras()[a].verificaDisponibilidade(b)){
        aux->getFileiras()[a].getAssentos()[b].setDisponibilidade(0);
        return &aux->getFileiras()[a].getAssentos()[b];
      }else{
        system("clear");
        cout << "Assento já ocupado..." << endl;
        system("sleep 2");
      }
  }while(flag == 1);
}

int VendaControleIngresso(Venda * venda, Sessao * sessao){
  int input;
  Assento * assentoAux = NULL;

  do{
    system("clear");
    cout << "1 - Adicionar ingressos" << endl;
    cout << "2 - Remover Ingressos" << endl;
    cout << "3 - Emitir Ingressos" << endl;
    cout << "Qualquer outra opção - Sai" << endl;
    cout << "Entrada: ";
    cin >> input;

    switch(input){
      case 1:
        system("clear");
        assentoAux = VendaControleEscolherAssento(sessao);
        venda->addIngresso(sessao, assentoAux);
        venda->calcularValorTotal();
        break;
      case 2:
        system("clear");
        venda->removerIngresso();
        break;
      case 3:
        system("clear");
        venda->emitirIngresso();
        cout << "Venda calculo total: " << venda->getValorTotal() << endl;
        system("sleep 3");
        return 1;
      default:
        input = 0;
        break;
    }
  }while(input);
}

int GerenciarSalas(Sala** salas, int * qtdSala)
{
  Sala * aux = NULL;
  int status;
  int input2;
  int i, input, qtdfil, nassentos;
  int indicesala;
  int nsala;

  system("clear");
  do
  {
    cout << "GERENCIAMENTO DE SALAS" << endl << endl;
    cout << "Selecione uma opcao:" << endl << endl << endl;

    cout << "1 - Adicionar uma sala" << endl;
    cout << "2 - Remover uma sala" << endl;
    cout << "3 - Alterar situacao de uma sala" << endl;
    cout << "4 - Exibir dados de uma sala" << endl;
    cout << "0 - Voltar ao menu inicial" << endl;
    cout << "Entrada: ";
    cin >> input;

    switch(input)
    {
      case 1:
        aux = NULL;
        indicesala = salas[*qtdSala]->getNumSala();

        for (i = 0; i < 50; i++)
        {
          if (salas[i]->getSituacao() != 0)
          {
            indicesala = i+1;
            break;
          }
        } //FOR PARA VERIFICAR SE NAO TEM NENHUM "BURACO" NO VETOR DE SALAS

        system("clear");
        cout << "Voce esta adicionando a sala " << indicesala << "." << endl;
        cout << "Insira a quantidade de fileiras dela." << endl;
        cin >> qtdfil;

        cout << "Insira a quantidade de assentos em cada fileira." << endl;
        cin >> nassentos;

        cout << "Insira a situacao dela." << endl;
        cout << "0. Disponivel" << endl;
        cout << "1. Manutencao de equipamento" << endl;
        cout << "2. Em reforma" << endl;
        cout << "3. Manutencao geral" << endl;
        cout << "Entrada: ";

        cin >> status;

        salas[indicesala - 1]->setNumSala(indicesala);
        salas[indicesala - 1]->setCapacidade(nassentos*qtdfil);
        salas[indicesala - 1]->setSituacao(status);
        salas[indicesala - 1]->setFileiras(qtdfil,nassentos);

        *qtdSala+=1;
        system("clear");
        break;

      case 2:
        cout << "Insira o numero da sala que deseja remover" << endl;
        cin >> nsala;

        if(salas[nsala-1]->getSituacao() == 4){
          system("clear");
          cout << "Sala inexistente!" << endl;
          system("sleep 2");
          system("clear");
          break;
        }

        salas[nsala - 1]->setSituacao(4);

        cout << "Sala " << nsala << "removida com sucesso." << endl;
        break;

      case 3:
        int situ;

        cout << "Insira o numero da sala que deseja alterar" << endl;
        cin >> nsala;

        if(salas[nsala-1]->getSituacao() == 4){
          system("clear");
          cout << "Sala inexistente!" << endl;
          system("sleep 2");
          system("clear");
          break;
        }

        cout << "Qual alteracao deseja fazer na sala " << nsala << "?" << endl << endl;
        cout << "Insira a situacao dela." << endl;
        cout << "0. Disponivel" << endl;
        cout << "1. Manutencao de equipamento" << endl;
        cout << "2. Em reforma" << endl;
        cout << "3. Manutencao geral" << endl;
        cout << "Entrada: ";
        cin >> situ;

        salas[nsala - 1]->setSituacao(situ);

        cout << "Alteracao na sala " << nsala << " feita com sucesso." << endl;
      break;

      case 4:
        cout << "Insira o numero da sala que deseja saber os dados" << endl;
        cin >> nsala;

        if(salas[nsala-1]->getSituacao() == 4){
          system("clear");
          cout << "Sala inexistente!" << endl;
          system("sleep 2");
          system("clear");
          break;
        }

        cout << "A sala "  << nsala
          << " possui capacidade de " << salas[nsala-1]->getCapacidade()
          << " lugares dispostos em " << salas[nsala-1]->getQtdFileiras()
          << " fileiras." << endl;

        if (salas[nsala - 1]->getSituacao() == 0)
        {
          cout << "A sala esta disponivel." << endl << endl;
        }
        else
        {
          cout << "A sala esta indisponivel." << endl << endl;
        }
        break;
      default:
       input = 0;
       break;
    }
  } while (input);
}


Sessao * VendaControleEscolherSessao(NoSessao ** listaSessao){

  NoSessao * listaAux = *listaSessao;
  Sessao * auxSessao = NULL;
  int input;
  int i;
  tm * horario;
  int disponivel;
  do{
    try{
      auxSessao = listaAux->info;
      horario = auxSessao->getHorario();
      cout << "Filme: " << auxSessao->getFilme() << endl;
      cout << "Horario: " << horario->tm_hour << "h " << horario->tm_min << "m" << endl;
      if(auxSessao->getStatus() == 0){
        cout << "Disponível: SIM" << endl;
        disponivel = 1;
      }else{
        cout << "Disponível: NÃO" << endl;
        disponivel = 0;
      }

      cout << "1 - Próxima Sessão" << endl;
      if(disponivel == 1)
        cout << "2 - Escolher Sessão" << endl;
      cout << "0 - Sair" << endl;

      cin >> input;

      if(input == 1){
        if(listaAux->prox != NULL){
          listaAux = listaAux->prox;
       }else{
         system("clear");
         cout << "Não há outra sessão." << endl;
         system("sleep 2");
         return NULL;
       }
      }else{
        if(input== 0){
          return NULL;
        }else{
          if((input == 2)&&(disponivel==1)){
            return auxSessao;
          }
        }
      }
    }catch(char const &erro){
      system("clear");
      cout << erro << endl;
      system("sleep 2");
      return NULL;
    }
  }while(auxSessao != NULL);
}

void GerenciarSessao(NoSessao **listaSessao, Sala **salas, int qtdSala){
  NoSessao *aux = NULL;
  tm *hr;
  Sala *salaAtual; //Para recuperar a sala da sessao.
  int input, numSala, flag=0, input2, input3;
  do{
    system("clear");
    cout << "\nGERENCIAMENTO DE SESSOES" << endl<< endl;
    cout << "Selecione uma opcao:" << endl;

    cout << "1 - Adicionar uma sessao" << endl;
    cout << "2 - Alterar dados de uma sessao" << endl;
    cout << "3 - Exibir dados das sessoes" << endl;
    cout << "0 - Voltar ao menu inicial" << endl;
    cout << "Entrada: ";
    cin >> input;

    switch(input){
      case 0:
      break;
      case 1://Inserir
        cout << "----------------Adicionar uma Sessao---------------------"<<endl;
        do{
          cout << "Em qual sala sera exibida a nova sessao?" << endl;
          try{ //Trata a exceçao do numero da sala ser errado 
            cin >> numSala;
            if((numSala <= 0)||(salas[numSala-1]->getSituacao() == 4)){
              throw "Erro! Sala inexistente";
            }
            else if(salas[numSala-1]->getSituacao() != 0) 
              throw "Essa sala esta indisponivel no momento";

            if((*listaSessao)==NULL){ //Verifica se lista esta vazia
              try{ //trata a exceçao de bad_aloc
                (*listaSessao) = new NoSessao();//Aloca espaco para um novo no na Lista
                (*listaSessao)->info = new Sessao(salas[numSala-1]);
              }catch(bad_alloc){
                cout << "Erro! Nao foi possivel criar uma nova sessao"<<endl;
              }
            }
            else{//Se a lista não estiver vazia, insere no fim dela.
              aux = *listaSessao;
              while(aux->prox != NULL){ //percorre ate o ultimo elemento
                aux = aux->prox;
              }
              try{//Trata a exceçao de bad_alloc
                aux->prox = new NoSessao();
                aux->prox->info = new Sessao(salas[numSala-1]);
              }catch(bad_alloc){
                cout <<"Erro! Nao foi possivel criar uma nova sessao"<<endl;
              }
            }
          }catch(const char *s){
            cout<< s <<endl;
            flag = 0;
          }
        }while(flag);
        break;
      case 2:
        cout << "----------------Alterar uma Sessao---------------------"<<endl;
        aux = (*listaSessao);
        do{
          if(aux != NULL){
            hr = aux->info->getHorario();
            salaAtual = aux->info->getSalaAtual();
            cout<<"\n----------------------------------------------------"<<endl;
            cout << "Sessao exibida na sala:" << salaAtual->getNumSala() << endl;
            cout << "Filme: " << aux->info->getFilme() << endl;
            cout << "Horario da Sessao: " << hr->tm_hour <<":"<< hr->tm_min<<"h"<<endl;
            if(!aux->info->getStatus()){ //Se a sessao nao esta encerrada
              cout << "A sessao esta disponivel"<<endl;
            }else{
              cout << "A sessao nao esta disponivel"<<endl;
            }
            cout<<"----------------------------------------------------"<<endl;
            cout << "1 - Editar essa sessao \t2 - Proxima sessao \t0 - Voltar ao menu inicial" << endl;
            cout << "Entrada: ";
            cin >> input2;
            switch(input2){
              case 0:
              break;
              case 1:
                editarSessao(aux->info, salas, qtdSala);
                input2 = 0;
              break;
              case 2: //Alterar o proximo filme
                if(aux->prox == NULL){
                  cout <<"Nao ha mais sessoes"<<endl;
                  input2 = 0;
                }
                else{
                  aux = aux->prox;
                }
              break;
              default:
                cout << "Opcao Invalida"<<endl;
              break;
            }
          }else{
            system("clear");
            cout << "Lista de sessoes vazia!" << endl;
            system("sleep 2");
            system("clear");
            input2 = 0;
          }
        }while(input2);
        break;
      case 3:
        aux = (*listaSessao);
        if(aux == NULL){
          system("clear");
          cout << "Lista de sessoes vazia!" << endl;
          system("sleep 2");
          system("clear");
          input2 = 0;
        }else{
          cout << "---------------------Exibir Sessoes---------------------"<<endl;
          aux = (*listaSessao);
          while(aux != NULL){
            exibirSessao(aux->info);
            aux = aux->prox;
          }
        }
        break;
      default:
        cout <<"Opcao Invalida."<<endl;
      break;
    }
  } while (input);
}

void editarSessao(Sessao *sessao, Sala **salas, int qtdSala){
  int entrada, numSala, flag=0;
  string nomeFilme;
  tm *hr;
  Sala *salaAtual;

  hr = sessao->getHorario();
  salaAtual = sessao->getSalaAtual();
  cout<<"SalaAtual:"<< salaAtual->getNumSala() <<endl;
  do{
    cout << "\n\nO que voce gostaria de editar?\n" << endl;
    cout << "1 - Sala de exibicao" << endl;
    cout << "2 - Filme exibido na sessao" << endl;
    cout << "3 - Horario da sessao" << endl;
    cout << "4 - Status da sessao" << endl;
    cout << "0 - Sair da edicao" <<endl;
    cin >> entrada;
    switch(entrada){
      case 0:
        break;
      case 1:
        do{
          try{
            cout <<"Digite a nova sala de exibicao: ";
            cin >> numSala;

            if((numSala <= 0)||(salas[numSala-1]->getSituacao() == 4)){
              system("clear");
              cout << "Sala inexistente!" << endl;
              system("sleep 2");
              system("clear");
            }else{
              if(salas[numSala-1]->getSituacao() != 0){
                system("clear");
                cout << "Sala indisponível." << endl;
                system("sleep 2");
                system("clear");
              }else{
                sessao->setSalaAtual(salas[numSala-1]);
              }
            }
          }catch(const char *s){
            cout<< s <<endl;
            flag = 0;
          }
        }while(flag);
        break;
      case 2:
        cout <<"Digite o novo nome:";
        cin >> nomeFilme;
        sessao->setFilme(nomeFilme);
        break;
      case 3:
        sessao->setHorario((*hr));
        break;
      case 4:
        cout <<"Digite 0 para Disponivel e 1 para encerrada"<<endl;
        sessao->setStatus();
        break;
      default:
        cout<<"Opcao Invalida"<<endl;
      break;
    }
  }while(entrada);
}

void exibirSessao(Sessao *sessao){
  tm *hr;
  hr = sessao->getHorario();

  cout << "Sessao exibida na sala:" << sessao->getSalaAtual()->getNumSala() << endl;
  cout << "Filme: " << sessao->getFilme() << endl;
  cout << "Horario da Sessao: " << hr->tm_hour <<":"<< hr->tm_min<<"h"<<endl;

  if(!sessao->getStatus()){ //Se a sessao nao esta encerrada
    cout << "A sessao esta disponivel"<<endl;
  }else{
    cout << "A sessao nao esta disponivel"<<endl<<endl;
  }
  cout<<"------------------------------------"<<endl<<endl;
}

//função para inicializar salas a partir de um arquivo
void inicializaSala(Sala ** salas){
 int i, i2;
 int capacidade, fileiras;

 if(verificaArquivo("sala.dat")){
  ifstream fsala("sala.dat");
  if(fsala.is_open()){
    int registros = -1;
    string linhaSala;

    while(getline(fsala,linhaSala,'\n')){
      registros+=1;
    }
    cout << "REGISTROS: " << registros << endl;

    //reseta ponteiro do arquivo para inicio (do arquivo)
    fsala.clear();
    fsala.seekg(0,ios::beg);

    int nassentos;
    for(i2=0; i2 < registros; i2++){
      for(i=0; i < 4; i++){
        if(i<3){
          getline(fsala, linhaSala, ',');
        }else{
          getline(fsala, linhaSala, '\n');
        }
        switch(i){
          case 0:
            salas[i2]->setNumSala(atoi(linhaSala.c_str()));
            break;
          case 1:
            capacidade = atoi(linhaSala.c_str());
            salas[i2]->setCapacidade(atoi(linhaSala.c_str()));
            break;
          case 2:
            salas[i2]->setSituacao(atoi(linhaSala.c_str()));
            break;
          case 3:
            fileiras = atoi(linhaSala.c_str());
            salas[i2]->setFileiras(atoi(linhaSala.c_str()),capacidade/fileiras);
            break;
          default:
            break;
        }

      }
    }
  }

  fsala.close();
 }
}

void gerarArquivo(NoVenda ** listaVenda){
  NoVenda * aux = *listaVenda;
  ofstream fvenda;
  tm data;

  if(aux != NULL){
    fvenda.open("relatorio.txt",ios::ate);
    while(aux != NULL){
      data = aux->info->getDtVenda();
      fvenda << "--------------------------------------" << endl;
      fvenda << "Venda - " << data.tm_mday << "/"
        << data.tm_mon << "/" << data.tm_year << endl;
      fvenda << "Dinheiro arrecadado: " << aux->info->getValorTotal() << endl;
      fvenda << "--------------------------------------" << endl;
      aux = aux->prox;
    }
    fvenda.close();
    system("clear");
    cout << "Relatório gerado..." << endl;
    system("sleep 2");
    system("clear");
  }else{
    system("clear");
    cout << "Não há vendas efetuadas... Fechando o program..." << endl;
    system("sleep 2");
    system("clear");
  }
}
