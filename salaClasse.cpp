/*
	UNIVERSIDADE FEDERAL DE SÃO CARLOS - CAMPUS SOROCABA
	Trabalho Integrado POO/ED1/ISI - 1º/2014

	Implementação dos métodos da classe Sala.h
*/

#include "salaClasse.h"
#include <iostream>
using namespace std;

Sala::Sala(int num, int qtdfil, int status , int assent){
	int i;
	char aux;

  switch(status){
    case 0:
      situacao = disponivel;
      break;
    case 1:
      situacao = manuEquipamento;
      break;
    case 2:
      situacao = reforma;
      break;
    case 3:
      situacao = manuGeral;
      break;
    default:
      situacao = desativada;
      break;
  }
	numSala = num;
	qtdFileiras = qtdfil;

	capacidade = qtdfil * assent; //Capacidade da sala é igual ao numero de assentos vezes o numero de fileiras
}
void Sala::setFileiras(int num, int nassentos) {
  int i;
  int aux;

  qtdFileiras = num;

  fileiras = new Fileira[qtdFileiras]; //Aloca vetor de fileiras
  //Inicializa os valores dos atributos de cada fileira da sala
  for (i = 0; i < qtdFileiras; i++){
    aux = 97+i; //Coloca o id de fileira de 'a' até ...
    fileiras[i].setIdFileira(aux);
    fileiras[i].setQtdAssentos(nassentos); // mudar
    fileiras[i].criaAssentos();
  }
}

Sala::~Sala(){
	delete [] fileiras; //desaloca o vetor de fileiras
}
int Sala::getNumSala() const{
	return numSala;
}

int Sala::getCapacidade() const{
	return capacidade;
}

int Sala::getSituacao() const{
	return situacao;
}

int Sala::getQtdFileiras() const{
	return qtdFileiras;
}

void Sala::setNumSala(int num){
	numSala = num;
}

void Sala::setCapacidade(int capac){
	capacidade = capac;
}

void Sala::setSituacao(int situ){
	situa = situ;
  switch(situ){
    case 0:
      situacao = disponivel;
      break;
    case 1:
      situacao = manuEquipamento;
      break;
    case 2:
      situacao = reforma;
      break;
    case 3:
      situacao = manuGeral;
      break;
    default:
      situacao = desativada;
      break;
  }
	
}

void Sala::setSituacao(Situacao situ){
  situacao = situ;
}

Fileira * Sala::getFileiras() const{
	return fileiras;
}

