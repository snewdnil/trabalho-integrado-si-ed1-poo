/*
UNIVERSIDADE FEDERAL DE SÃO CARLOS - CAMPUS SOROCABA
Trabalho Integrado POO/ED1/ISI - 1º/2014

Código referente a classe Sala.
*/
#ifndef SALA_H
#define SALA_H

#include "fileiraClasse.h"

class Sala{

private:
	int numSala;
	int capacidade;
	enum Situacao{
		disponivel = 0,
		manuEquipamento,
		reforma,
		manuGeral,
		desativada
	};
  Situacao situacao;
	int situa;
	Fileira *fileiras; //Vetor de fileiras da sala
	int qtdFileiras; //Numero de fileiras da sala; 

public:
	Sala(int num, int qtdfil, int status, int assent); //Construtor da sala 
	~Sala(); //Destrutor --desaloca o vetor de fileiras
	int getNumSala() const; //Retorna o Numero da Sala
	int getCapacidade() const; //Retorna a Capacidade da Sala
	int getSituacao() const; //Retorna a Situação da Sala (de acordo com o Enum)
	int getQtdFileiras() const;
	Fileira * getFileiras() const; //Retorna o vetor de fileiras
	void setNumSala(int num); //Muda o Numero da Sala
	void setCapacidade(int capac); // Muda a capacidade da Sala
	void setSituacao(int situ); //Muda a situação da Sala
  void setSituacao(Situacao situ); //sobrecarga para mudar a situação da sala
  void setFileiras(int num, int nassentos);
};

#endif
