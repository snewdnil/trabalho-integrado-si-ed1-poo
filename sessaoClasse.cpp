#include "sessaoClasse.h"

Sessao::Sessao(Sala * salaParam){
  int i;

  encerrada = 0;
  numVendido = 0;

  cout << "Qual é o filme que será exibido na Sessao?" << endl;
  cin >> Sessao::filme;
  cout << "Digite as horas" << endl;
  cin >> horario.tm_hour;
  cout << "Digite os minutos" << endl;
  cin >> horario.tm_min;

  sala = salaParam;
}

string Sessao::getFilme()
{
  return filme;
}
void Sessao::setFilme(string nomeFilme){
  filme = nomeFilme;
}

// essa função no caso verifica se esta disponivel o número de vagas
// numero de vagas total - número vendidos...
int Sessao::getDisponivel()
{
  int capacidadeSala = sala->getCapacidade();
  if(capacidadeSala - numVendido <= 0){
    return 0;
  }else{
    return 1;
  }
}

Sala * Sessao::getSalaAtual(){
  return sala;
}

void Sessao::setnumVendido(int e)
{
  numVendido = e;
}
int Sessao::getStatus()
{
  return encerrada;
}

void Sessao::setStatus()
{
  cin >> encerrada;
}

tm * Sessao::getHorario(){
  return &horario;
}

void Sessao::setHorario(tm & horaElemento)
{
  cout << "Digite as horas" << endl;
  cin >> horaElemento.tm_hour;
  cout << "Digite os minutos" << endl;
  cin >> horaElemento.tm_min;
}

void Sessao::setSalaAtual(Sala *salaAtual){
  sala = salaAtual;
}
