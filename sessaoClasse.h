#ifndef SESSAO_H
#define SESSAO_H

#include <iostream>
#include "salaClasse.h"

//using namespace string;
using namespace std;

class Sessao{
  private:
    int encerrada;
    int numVendido;
    string filme;
    tm  horario; //tm é uma estrutura de dados para data

    // relação simples no qual sessão aloca sala.
    Sala * sala;

  public:
    Sessao(Sala * salaParam);
    string getFilme();
    void setFilme(string nomeFilme);
    int getDisponivel();
    Sala * getSalaAtual();
    void setSalaAtual(Sala *salaAtual);
    void setnumVendido(int e);
    int getStatus();
    void setStatus();
    tm * getHorario();
    void setHorario(tm & horaElemento);
};

#endif

