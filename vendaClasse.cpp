#include "vendaClasse.h"

using namespace std;

Venda::Venda(Sessao * sessaoParam){
  char input;
  t = time(0);
  dtVenda = localtime(&t);
  dtVenda->tm_mon+=1;
  dtVenda->tm_year+=1900;

  cout << "DIA: " << dtVenda->tm_mday << endl;
  cout << "MES: " << dtVenda->tm_mon << endl;
  cout << "ANO: " << dtVenda->tm_year << endl;
  cout << "Confirma data da venda? [s/n] : ";

  cin >> input;
  if(input != 's'){
    cout << "Por favor, entre com a data correta." << endl;
    cout << "DIA: "; cin >> Venda::dtVenda->tm_mday;
    cout << "MES: "; cin >> Venda::dtVenda->tm_mon;
    cout << "ANO: "; cin >> Venda::dtVenda->tm_year;
  }

  ingresso = NULL;

  valorTotal = 0.00;
  sessao = sessaoParam;
}
Venda::~Venda(){

}

double Venda::calcularValorTotal(){
  NoIngresso * aux = ingresso;
  double acumulador = 0.00;

  if(aux == NULL){
    return 0.00;
  }else{
    while(aux!=NULL){
      acumulador+=aux->info->getValor();
      aux = aux->prox;
    }
  }
  valorTotal = acumulador;
  return acumulador;
}

void Venda::emitirIngresso(){
  int vendidos = 0;
  if(ingresso == NULL){
    cout << "Nao há mais ingressos para serem emitidos." << endl;
    return;
  }
  NoIngresso* aux = ingresso;

  while(aux!= NULL){
    vendidos+=1;
    aux = aux->prox;
  }
  sessao->setnumVendido(vendidos);

  aux = ingresso;
  NoIngresso * aux2 = ingresso->prox;
  while(aux != NULL){
    delete aux->info;
    aux2 = aux;
    aux = aux->prox;
    free(aux2);
  }
  ingresso = NULL;
  // atualiza sessao
}

void Venda::addIngresso(Sessao * sessao, Assento * assento){
  NoIngresso * aux = NULL;
  NoIngresso * aux2 = NULL;

  // verifica se a lista esta vazia
  if(ingresso == NULL){
    ingresso = new NoIngresso;
    ingresso->info = new Ingresso(assento,sessao);
  }else{
    // caso não esteja, ele vai andar
    // até o último ingresso para
    // inserir posteriormente
    aux = ingresso;
    while(aux->prox != NULL){
      aux = aux->prox;
    }
    aux2 = new NoIngresso;
    aux2->info = new Ingresso(assento,sessao);
    aux->prox = aux2;
  }
}

void Venda::removerIngresso(){
  tm data;
  int input;
  system("clear");
  if(ingresso == NULL){
    cout << "Não há ingressos para serem removidos." << endl;
    system("sleep 2");
    return;
  }else{
    NoIngresso * aux = ingresso;
    NoIngresso * auxAnterior;
    while(aux!= NULL){
      system("clear");
      data = aux->info->getDtIngresso();
      cout << "Ingresso adicionado no dia: ";
      cout << data.tm_mday << "/" << data.tm_mon << "/"
        << data.tm_year;
      cout << " às " << data.tm_hour << "h e " << data.tm_min << endl;
      cout << "Valor: " << aux->info->getValor() << endl;

      cout << "1 - Remover Ingresso" << endl;
      cout << "2 - Ir para o próximo Ingresso" << endl;
      cout << "Outras teclas - Sair" << endl;
      cin >> input;
      if(input == 1){
        if(aux == ingresso){
          auxAnterior = aux;
          auxAnterior->info->getAssentoAtual()->setDisponibilidade(1);
          ingresso = aux->prox;
          delete auxAnterior;
        }else{
          auxAnterior->info->getAssentoAtual()->setDisponibilidade(1);
          auxAnterior->prox = aux->prox;
          delete aux;
        }
        return;
      }else{
        if(input == 2){
          auxAnterior = aux;
          aux = aux->prox;
          if(aux == NULL){
            system("clear");
            cout << "Não há mais ingressos..." << endl;
            system("sleep 2");
            system("clear");
          }
        }else{
          return ;
        }
      }
    }
  }
}
tm Venda::getDtVenda(){
  return *dtVenda;
}

double Venda::getValorTotal(){
  return valorTotal;
}

Sessao * Venda::getSessao(){
  return sessao;
}


