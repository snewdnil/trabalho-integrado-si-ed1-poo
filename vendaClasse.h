/*UNIVERSIDADE FEDERAL DE SÃO CARLOS - CAMPUS SOROCABA
Trabalho Integrado POO/ED1/ISI - 1º/2014

Código referente a classe Assento.
*/
#ifndef VENDA_H
#define VENDA_H

#include "ingressoClasse.h"
#include <cstdlib>
#include <ctime> // biblioteca para pegar a data atual
class Venda{
    private:
    typedef struct noingresso{
      Ingresso * info;
      struct noingresso *prox;
      // construtor da struct
      noingresso(){
        prox = NULL;
        info = NULL;
      }
    }NoIngresso;


    enum pagamento{
      dinheiro,
      cartao
    };

    time_t t;
    tm * dtVenda;

    double valorTotal;

    NoIngresso * ingresso;
    Sessao * sessao;

  public:
    Venda(Sessao * sessaoParam);
    ~Venda();
    double calcularValorTotal();
    void emitirIngresso();
    void addIngresso(Sessao * sessao, Assento * assento);
    void removerIngresso();
    Sessao * getSessao();

    double getValorTotal();
    tm getDtVenda();
};
#endif

